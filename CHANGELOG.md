# Changelog

## 4.0.0 - 2023-12-25
* Adding support for PHP 8.3
* Requiring Cilician 4.0
* Adding support for Symfony 7
* Removing support for Symfony 4

## 3.0.1 - 2023-08-25
* Adding API key param to HaveIBeenPwnedApiClient in services.xml

## 3.0.0 - 2022-08-08
* PHP 8 support
* Symfony 6 support

## 1.0.0 - 2022-08-08
* Dropping support for Symfony 3
* Allowing v5 of `symfony/framework-bundle` and `symfony/validator`

## 0.1.0
* Initial implementation
