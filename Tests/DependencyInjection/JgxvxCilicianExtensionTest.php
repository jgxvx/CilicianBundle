<?php

/*
 * This file is part of CilicianBundle.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianBundle\Tests\DependencyInjection;

use Jgxvx\CilicianBundle\DependencyInjection\JgxvxCilicianExtension;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

/**
 * @coversNothing
 */
class JgxvxCilicianExtensionTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function testIfExtensionIsLoadedWithoutError(): void
    {
        $extension = new JgxvxCilicianExtension();
        $container = \Mockery::mock(ContainerBuilder::class);
        $container->shouldReceive('hasExtension')->once()->andReturnFalse();
        $container->shouldReceive('fileExists')->once()->andReturnFalse();
        $container->shouldReceive('getParameterBag')->once()->andReturn(new ParameterBag());
        $container->shouldReceive('setDefinition')->atLeast()->once()->andReturn();
        $container->shouldReceive('setAlias')->once()->andReturn();
        $container->allows('removeBindings')->with(\Mockery::type('string'))->andReturns();

        $extension->load([], $container);

        $this->assertTrue(true);
    }
}
