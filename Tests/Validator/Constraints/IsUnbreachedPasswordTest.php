<?php

/*
 * This file is part of CilicianBundle.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianBundle\Tests\Validator\Constraints;

use Jgxvx\CilicianBundle\Validator\Constraints\IsUnbreachedPassword;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\CilicianBundle\Validator\Constraints\IsUnbreachedPassword
 */
class IsUnbreachedPasswordTest extends TestCase
{
    public function testIfValidatorIsConfigured(): void
    {
        $constraint = new IsUnbreachedPassword();

        $this->assertSame('cilician_validator_is_unbreached_password', $constraint->validatedBy());
    }
}
