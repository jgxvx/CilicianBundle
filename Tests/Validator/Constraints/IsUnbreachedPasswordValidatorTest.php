<?php

/*
 * This file is part of CilicianBundle.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianBundle\Tests\Validator\Constraints;

use Jgxvx\Cilician\Exception\RateLimitException;
use Jgxvx\Cilician\Result\PasswordCheckResult;
use Jgxvx\Cilician\Service\Cilician;
use Jgxvx\CilicianBundle\Validator\Constraints\IsUnbreachedPassword;
use Jgxvx\CilicianBundle\Validator\Constraints\IsUnbreachedPasswordValidator;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

/**
 * @covers \Jgxvx\CilicianBundle\Validator\Constraints\IsUnbreachedPasswordValidator
 */
class IsUnbreachedPasswordValidatorTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public static function getPasswords(): array
    {
        return [
            ['foo'],
            ['bar'],
        ];
    }

    public static function getEmptyValues(): array
    {
        return [
            [null],
            [''],
        ];
    }

    public static function getNonStringValues(): array
    {
        return [
            [1],
            [1.2],
            [[]],
        ];
    }

    /**
     * @dataProvider getPasswords
     */
    public function testIfValidValuesAreValidated(string $value): void
    {
        $cilician   = $this->createCilicianDummy();
        $validator  = new IsUnbreachedPasswordValidator($cilician, new NullLogger());
        $constraint = new IsUnbreachedPassword();
        $context    = $this->createContextDummy();

        $cilician->shouldReceive('checkPassword')->once()->andReturn($this->createSuccessResult($value));
        $context->shouldReceive('buildViolation')->never();

        $validator->initialize($context);

        try {
            $validator->validate($value, $constraint);
        } catch (UnexpectedTypeException $ex) {
            $this->fail('Unexpected exception: ' . $ex->getMessage());
        }

        $this->assertTrue(true);
    }

    /**
     * @dataProvider getEmptyValues
     */
    public function testIfEmptyValuesAreIgnored(string $value = null): void
    {
        $cilician   = $this->createCilicianDummy();
        $validator  = new IsUnbreachedPasswordValidator($cilician, new NullLogger());
        $constraint = new IsUnbreachedPassword();
        $context    = $this->createContextDummy();

        $cilician->shouldReceive('checkPassword')->never();
        $context->shouldReceive('buildViolation')->never();

        $validator->initialize($context);

        try {
            $validator->validate($value, $constraint);
        } catch (UnexpectedTypeException $ex) {
            $this->fail('Unexpected exception: ' . $ex->getMessage());
        }

        $this->assertTrue(true);
    }

    /**
     * @dataProvider getNonStringValues
     *
     * @param mixed $value
     */
    public function testIfNonStringValuesAreDetected($value): void
    {
        $cilician   = $this->createCilicianDummy();
        $validator  = new IsUnbreachedPasswordValidator($cilician, new NullLogger());
        $constraint = new IsUnbreachedPassword();

        $cilician->shouldReceive('checkPassword')->never();
        $this->expectException(UnexpectedTypeException::class);

        $validator->validate($value, $constraint);
    }

    /**
     * @dataProvider getPasswords
     */
    public function testIfCilicianExceptionIsCaught(string $value): void
    {
        $cilician   = $this->createCilicianDummy();
        $validator  = new IsUnbreachedPasswordValidator($cilician, new NullLogger());
        $constraint = new IsUnbreachedPassword();

        $cilician->shouldReceive('checkPassword')->andThrow(new RateLimitException(2));

        $validator->validate($value, $constraint);

        try {
            $validator->validate($value, $constraint);
        } catch (\Throwable $ex) {
            $this->fail('Unexpected exception: ' . $ex->getMessage());
        }

        $this->assertTrue(true);
    }

    /**
     * @dataProvider getPasswords
     */
    public function testIfPwnedPasswordsCreateViolations(string $value): void
    {
        $cilician          = $this->createCilicianDummy();
        $validator         = new IsUnbreachedPasswordValidator($cilician, new NullLogger());
        $constraint        = new IsUnbreachedPassword();
        $context           = $this->createContextDummy();
        $constraintBuilder = $this->createViolationBuilderDummy();

        $cilician->shouldReceive('checkPassword')->once()->andReturn(new PasswordCheckResult($value, 1));
        $constraintBuilder->shouldReceive('setParameter')->twice()->andReturnSelf();
        $constraintBuilder->shouldReceive('addViolation')->once();
        $context->shouldReceive('buildViolation')->once()->andReturn($constraintBuilder);

        $validator->initialize($context);
        $validator->validate($value, $constraint);

        $this->assertTrue(true);
    }

    /**
     * @return Cilician|\Mockery\MockInterface
     */
    private function createCilicianDummy()
    {
        return \Mockery::mock(Cilician::class);
    }

    private function createSuccessResult(string $password): PasswordCheckResult
    {
        return new PasswordCheckResult($password, 0);
    }

    /**
     * @return ExecutionContextInterface|\Mockery\MockInterface
     */
    private function createContextDummy()
    {
        return \Mockery::mock(ExecutionContextInterface::class);
    }

    /**
     * @return ConstraintViolationBuilderInterface|\Mockery\MockInterface
     */
    private function createViolationBuilderDummy()
    {
        return \Mockery::mock(ConstraintViolationBuilderInterface::class);
    }
}
