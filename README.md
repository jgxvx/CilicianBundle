# CilicianBundle

Bundle to integrate **cilician**, a [haveibeenpwned.com][4] API client for PHP, into Symfony applications.

[![pipeline status](https://gitlab.com/jgxvx/cilicianbundle/badges/master/pipeline.svg)](https://gitlab.com/jgxvx/cilician/commits/master)
[![coverage report](https://gitlab.com/jgxvx/cilicianbundle/badges/master/coverage.svg?job=test)](https://gitlab.com/jgxvx/cilician/commits/master)

## Installation

```console
$ composer require jgxvx/cilician-bundle
```

## Documentation

This bundle wraps the [Cilician][1] library. Please refer to the library documentation to learn what you can do with it.

### Services

The bundle provides two public services: The main library service, `cilician`, and the password validator `cilician.validator.isunbreachedpassword`.

#### cilician Service

Type-hint your parameter with `Jgxvx\Cilician\Service\Cilician` or require the service by its alias `cilician` to inject the service into your components. 

#### Password Validator & Constraint

To prevent users from choosing a password that has appeared in a documented data breach, add an `IsUnbreachedPassword` constraint to your signup form:

```php
$builder
    ->add('password', PasswordType::class, [
        'constraints' => [
            new IsUnbreachedPassword(),
        ],
        'label' => 'label.password',
        'attr' => [
            'autocomplete' => 'off',
        ],
    ]);
```

There is also the possibility to add a constraint assertion to a property. However, this should be used with extreme caution: Make sure to not save plaintext passwords in your database. Also, if used with a User entity, this will most likely not work, because the password property will hold a password hash.

```php
use Jgxvx\CilicianBundle\Validator\Constraints as CilicianAssert;

// ...

/**
 * @var string
 *
 * @CilicianAssert\IsUnbreachedPassword()
 */
private $password;
```

#### Caching

If a PSR-16 compatible caching service is available under the `cache` alias, it will be injected into `cilician` to enable caching.

#### Logging

If a PSR-3 compatible logger is available under the `logger` alias, it will be injected into `cilician` to enable logging.

## License

CilicianBundle is open source software published under the MIT license. Please refer to the [LICENSE][3] file for further information.

## Contributing

Please refer to the [Contribution Guidelines][2].

[1]: https://gitlab.com/jgxvx/cilician
[2]: https://gitlab.com/jgxvx/cilician-bundle/CONTRIBUTING.md
[3]: https://gitlab.com/jgxvx/cilician-bundle/LICENSE
[4]: https://haveibeenpwned.com