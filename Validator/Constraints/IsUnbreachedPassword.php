<?php

/*
 * This file is part of CilicianBundle.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsUnbreachedPassword extends Constraint
{
    public string $message = 'The password "{{ password }}" has appeared in documented data breaches {{ times }}. Please choose another password.';

    public function validatedBy(): string
    {
        return 'cilician_validator_is_unbreached_password';
    }
}
