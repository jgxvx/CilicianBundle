<?php

/*
 * This file is part of CilicianBundle.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\CilicianBundle\Validator\Constraints;

use Jgxvx\Cilician\Exception\CilicianException;
use Jgxvx\Cilician\Service\Cilician;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class IsUnbreachedPasswordValidator extends ConstraintValidator implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    private Cilician $cilician;

    public function __construct(Cilician $cilician, LoggerInterface $logger = null)
    {
        $this->cilician = $cilician;
        $this->logger   = $logger;
    }

    /**
     * @param Constraint|IsUnbreachedPassword $constraint
     *
     * @throws UnexpectedTypeException
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (null === $value || '' === $value) {
            return;
        }

        if (!\is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        try {
            $result = $this->cilician->checkPassword($value);
        } catch (CilicianException $ex) {
            $this->logger?->error('Could not validate password: ' . $ex->getMessage());

            return;
        }

        if ($result->isPwned()) {
            $count = $result->getCount();

            if (1 === $count) {
                $times = '1 time';
            } else {
                $times = "{$count} times";
            }

            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ password }}', $value)
                ->setParameter('{{ times }}', $times)
                ->addViolation();
        }
    }
}
